with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "scenic-dev";

  nativeBuildInput = [
    binutils gcc gnumake openssl pkgconfig #common deps
  ];

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = [
    git less vim # common tools
    nodejs-14_x
    jq
  ];

  shellHook = ''
    NODE_PATH="/tmp/node_modules";

    export PATH="$PATH:$NODE_PATH/bin";
    npm config set prefix "$NODE_PATH"

    if [ ! -e "$NODE_PATH/bin/standard" ]; then
       npm install standard --global
    fi

    echo "Development environment with NodeJS $(node --version) is ready!"
  '';
}
