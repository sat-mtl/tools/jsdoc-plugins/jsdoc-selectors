# jsdoc-selectors

A JSDoc plugin that parses all `@selector` tags and generates a pretty Markdown page with all the selectors sorted by modules.

The result of this plugin is intended to be used by the QA team 💖 that uses CSS selectors in order to make automated tests for the Front-End applications.

## Getting started
### Install
Install the plugin from npm and the `@sat-valorisation` group:

```bash
npm i --save-dev @sat-valorisation/jsdoc-selectors
```

### Configure
And then configure the plugin from the JSDoc configuration script:

```json
{
  "plugins": [
    "@sat-valorisation/jsdoc-selectors"
  ],
  "selectorGlossary": {
    "destination": "GLOSSARY.md",
    "title": "Selector glossary"
  }
}
```

The plugin accepts the following parameters under the `selectorGloassary` key:

| Parameters       | Description                                                                               | Default value        |
| ----------       | -------------------------------------------------                                         | -------------        |
| **title**        | Change the title of the generated Markdown file.                                          | `Selectors glossary` |
| **desctination** | Relative path (from where the `jsdoc` command is executed) where the glossary is executed | `./GLOSSARY.md`      |

## Example

In order to test and experiment this plugin, execute `jsdoc` from the the `example` directory:

```bash
cd example
npx jsdoc -c conf.json
```

You should see the `destination.md` file that should contain all the documented selectors.

## Aknowledgement

+ [Doctrine](https://github.com/eslint/doctrine), a JSDoc parser that is **unmaintained**. It seems we should investigate [ESLint JSDoc plugin](https://github.com/gajus/eslint-plugin-jsdoc) in order to reduce potential issues with this project.
+ [Tablemark](https://github.com/citycide/tablemark), a JSON to Markdown tables generator.
